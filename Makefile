setup:
	wget http://cloud-images.ubuntu.com/trusty/current/trusty-server-cloudimg-amd64-disk1.img
	sudo apt install qemu libvirt-daemon-system virtinst

# echo 'allow virbr0' > /etc/qemu/bridge.conf
build:
	qemu-img create -b trusty-server-cloudimg-amd64-disk1.img -f qcow2 -F qcow2 core.img 10G
	genisoimage -output cidata.iso -V cidata -r -J user-data meta-data
	sudo virt-install --name=core --ram=2048 --vcpus=1 --import --disk path=core.img,format=qcow2 --disk path=cidata.iso,device=cdrom --os-variant=ubuntu14.04 --network bridge=virbr0,model=virtio --graphics vnc,listen=0.0.0.0 --noautoconsole

list:
	@sudo virsh -c qemu:///system "list --all"

status:
	@sudo virsh -c qemu:///system net-dhcp-leases default

clean:
	@sudo virsh -c qemu:///system destroy core
	@sudo virsh -c qemu:///system undefine core --remove-all-storage
	#@rm -f *.img *.iso
